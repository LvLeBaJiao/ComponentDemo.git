package com.panxc.framework

/**
 * 组件生命周期接口
 * Created by panxc on 2019-06-17.
 */

interface IApplicationLike {

    fun onCreate()

    fun onStop()
}
